import { Component } from '@angular/core';

@Component({
    templateUrl: './templates/404.component.html',
})

export class PageNotFoundComponent { }
