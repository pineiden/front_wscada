import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        let datatypes = [
            {
                id: 1,
                name: "ESTO",
                csk: "[ESTO|[a|[q|[(b|c)=(d|e)]]]=[f|[(g|h)]]]",
                description: "Ejemplo corto",
                graph: ''
            },
            {
                id: 2,
                name: "DOTO",
                csk: "[DOTO|[(a|b)=(c|d)=(q|g)]]",
                description: "Ejemplo con ramas paralelas definidas",
                graph: ''

            },
            {
                id: 3,
                name: "PIGRO",
                csk: "[PIGRO|[a|[b|[c=d]]]=[e|[(f|g)=h]]=[i|[j|[k|[l=m]]]]]",
                description: "ejemplo complejo",
                graph: ''

            },

        ];
        return { datatypes };
    }
}
