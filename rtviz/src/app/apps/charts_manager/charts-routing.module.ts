import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChartsComponent } from './charts.component'

//Chart Components
import { AddChartComponent } from './components/chart/add.component';
import { ListChartsComponent } from './components/chart/list.component';
import { ViewChartComponent } from './components/chart/view.component';
//Chart's Creator Components
import { AddChartCreatorComponent } from './components/chart_creator/add.component';
import { ListChartsCreatorsComponent } from './components/chart_creator/list.component';
import { ViewChartCreatorComponent } from './components/chart_creator/view.component';

//import { CreateCSKComponent } from './create_csk.component';
//import { ListDataTypesComponent } from './list_datatypes.component';
//import { DataTypeComponent } from './datatype.component';

const routes: Routes = [
    {
        path: 'charts',
        component: ChartsComponent,
        children: [
            {
                path: '',
                children: [
                    { path: '', redirectTo: 'charts/list', pathMatch: 'full' },
                    // Charts URLS
                    { path: 'chart/add', component: AddChartComponent },
                    { path: 'charts/list', component: ListChartsComponent },
                    { path: 'chart/detail/:id', component: ViewChartComponent },
                    // Chart's Creator URLS
                    { path: 'creator/add', component: AddChartCreatorComponent },
                    { path: 'creators/list', component: ListChartsCreatorsComponent },
                    { path: 'creator/detail/:id', component: ViewChartCreatorComponent },

                ]

            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ChartRoutingModule { }

