import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SciInputComponent } from './sci_input.component';
//DataTypeComponent
import { CreateCSKComponent } from './components/datatype/add.component';
import { ListDataTypesComponent } from './components/datatype/list.component';
import { DataTypeViewComponent } from './components/datatype/view.component';
import { DataTypeComponent } from './components/datatype/main.component';

const routes: Routes = [
    {
        path: 'sci_input',
        component: SciInputComponent,
        children: [
            {
                path: '',
                children: [
                    { path: '', redirectTo: '/', pathMatch: 'full' },
                    // Datatype component
                    { path: 'datatype/create', component: CreateCSKComponent },
                    { path: 'datatype/update/:id', component: CreateCSKComponent },
                    { path: 'datatypes/list', component: ListDataTypesComponent },
                    { path: 'datatype', component: DataTypeComponent },
                    { path: 'datatype/view/:id', component: DataTypeViewComponent }
                ]

            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class SciInputRoutingModule { }

