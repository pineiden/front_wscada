import {
    Component, OnInit,
    ViewEncapsulation,
    OnChanges, SimpleChanges,
    ChangeDetectionStrategy,
} from '@angular/core';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';



@Component({
    selector: 'test-x-tick',
    providers: [Location, { provide: LocationStrategy, useClass: HashLocationStrategy }],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './test-x-tick.component.html',
    styleUrls: ['./test-x-tick.component.scss']
})

export class TestXTickComponent implements OnInit, OnChanges {
    width: number = 600;
    height: number = 600;
    label = "Test Tick";
    x_position = 250;
    font_size_label = 25
    axis_x = {}
    axis_y = {}
    central_y_position: number;
    left_x_val = 0;
    right_x_val = 0;
    y1 = 0;
    y2 = 0;

    tick_grid = {}
    tick_line = {}

    pos_text: {
    };

    tick_length: number = 30;


    ngOnInit() {
        console.log("Test tick x axis")
        this.setYaxis()
        this.setXaxis()
        this.set_y_position()
        this.getTickSize()
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.getTickSize()
    }


    setYaxis() {
        this.axis_y = {
            x1: 20,
            y1: 20,
            x2: 20,
            y2: this.height - 100
        }
    }

    setXaxis() {
        this.axis_x = {
            x1: 20,
            y1: this.axis_y['y2'] / 2 + 20,
            x2: this.width - 20,
            y2: this.axis_y['y2'] / 2 + 20
        }
    }

    set_y_position() {
        this.y1 = this.axis_y["y1"]
        this.y2 = this.axis_y["y2"]
    }


    getTickSize() {
        console.log("new tick, resize")
        console.log(this.y1, this.y2)
        // tick grid
        this.tick_grid = {
            x1: this.x_position,
            y1: this.y1,
            x2: this.x_position,
            y2: this.y2 - 50
        }
        // tick line
        this.tick_line = {
            x1: this.x_position,
            y1: this.y2,
            x2: this.x_position,
            y2: this.y2 + this.tick_length
        }
        this.pos_text = {
            x: this.x_position,
            y: this.y2 + this.tick_length + this.font_size() + 5,
        }

        //this.get_left_x()
        //this.get_rigth_x()
    }

    font_size() {
        return this.font_size_label
    }

    minus_font() {
        this.font_size_label = this.font_size_label - 1
    }

    plus_font() {
        this.font_size_label = this.font_size_label + 1
    }

    get_left_x() {
        console.log("left x")
        console.log(this.tick_grid["x1"])
        this.left_x_val = this.tick_grid["x1"]
    }

    get_rigth_x() {
        console.log("right x")
        console.log(this.tick_grid["x2"])
        this.right_x_val = this.tick_grid["x2"]
    }

    left_x() {
        this.x_position = this.x_position - 1
    }

    right_x() {
        this.x_position = this.x_position + 1
    }

    up_y() {
        console.log("SET UP")
        this.y1 = this.y1 - 1
        this.y2 = this.y2 - 1
        this.axis_y["y1"] = this.y1
        this.axis_y["y2"] = this.y2
        this.axis_x["y1"] = this.y2
        this.axis_x["y2"] = this.y2
        //this.getTickSize()

    }

    down_y() {
        console.log("SET DOWN")
        this.y1 = this.y1 + 1
        this.y2 = this.y2 + 1
        this.axis_y["y1"] = this.y1
        this.axis_y["y2"] = this.y2
        this.axis_x["y1"] = this.y2
        this.axis_x["y2"] = this.y2
        //this.getTickSize()

    }



}
