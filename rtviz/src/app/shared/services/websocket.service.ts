import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Observer } from "rxjs/Observer";


import { Message } from '../elements/message'

//import { } from 'uuws';
// source: https://github.com/elliotforbes/angular-websockets/blob/master/src/app/websocket.service.ts


@Injectable()
export class WebSocketService {
    private subject: Subject<MessageEvent>;
    private subjectData: Subject<number>;

    public connect(url: string): Subject<MessageEvent> {
        if (!this.subject) {
            this.subject = this.create(url)
        }
        return this.subject
    }

    private create(url: string): Subject<MessageEvent> {
        let ws = new WebSocket(url);
        console.log("Conexión a ")
        console.log(url)

        let observable = Observable.create(
            (obs: Observer<MessageEvent>) => {
                ws.onmessage = obs.next.bind(obs);
                ws.onerror = obs.error.bind(obs);
                ws.onclose = obs.complete.bind(obs);

                return ws.close.bind(ws);
            }
        );
        let observer = {
            next: (data: Object) => {
                if (ws.readyState == WebSocket.OPEN) {
                    ws.send(JSON.stringify(data))
                }
            }
        };

        return Subject.create(observer, observable)
    }


}
