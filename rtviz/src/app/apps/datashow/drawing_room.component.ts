import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { WebSocketService } from '../../shared/services/websocket.service';
import { DataBindingService } from './services/data_binding.service';

import { Message } from '../../shared/elements/message'

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/first';

import { Subject } from 'rxjs/Subject';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

import { Viz } from 'd3plus-viz';


// Charts imports

import * as shape from 'd3-shape';
import * as d3 from 'd3';

import { GPS } from "../utils/gps"
import { gps_list } from '../utils/data'

import { ColorPickerService } from 'angular4-color-picker';

const Nidm = 5;

@Component({
    selector: 'datashow-main',
    templateUrl: './templates/drawing_room.component.html',
    styleUrls: ['./static/scss/drawing_room.styles.scss'],
    providers: [
        WebSocketService,
        DataBindingService]
})
export class StationDataViewComponent implements OnInit {
    title = "Station's Data Reception"
    data: any[];
    stations: string[] = []

    public idm: string;

    // the amount of seconds to control the length of the list
    public dt: number = 600;
    private accepted: boolean;

    /*
      These are the chart definitions
    */

    // how the timeserie chart works
    chart_settings = {
        'mode': { 'chart': 'real_time', 'movement': 'dynamic' },
        'update': 600
    }

    // size by svg

    width: number = 600;
    height: number = 100;
    view: any[];

    // chart_name

    chart_name: string = 'time-serie';


    // gps list data: --> start

    gps_data: GPS[];
    data_serie: {};
    amount_data: number = 10;

    realTimeData: boolean = true;
    count = 0;

    set_titles = {
        N: {
            axis_x: { label: "Time", font_size: 18 },
            axis_y: { label: "N", font_size: 18 },
            chart: { label: "GPS North", font_size: 22 }
        },

        E: {
            axis_x: { label: "Time", font_size: 18 },
            axis_y: { label: "E", font_size: 18 },
            chart: { label: "GPS East", font_size: 22 }
        },

        U: {
            axis_x: { label: "Time", font_size: 18 },
            axis_y: { label: "U", font_size: 18 },
            chart: { label: "GPS Up", font_size: 22 }
        }
    }

    linearScale: boolean = false;
    fitContainer: boolean = false;
    tohtml: string = ''

    // additional sharp

    figure_type: string = "circle"

    properties = {
        cx: 0,
        cy: 0,
        r: 20,
        fill: "red",
        stroke: "gray",
        stroke_width: 3,
    }
    position_x_y = {
        x: 10,
        y: 20,
    }

    shape_position = {
        index_x: 0,
        index_y: 0,
        chart_x: 0,
        chart_y: 0,
    }

    new_shape_position = 0;

    // adding new shape
    select_figures = [
        { value: 'circle', name: 'Círculo' },
        { value: 'square', name: 'Cuadrado' }]
    selected_figure: string;
    new_shape_figure: string;
    new_shape = { radius: 5, side_length: 5 / Math.sqrt(2) }
    shape_list: any[] = [];

    send_figure_N: {};
    send_figure_E: {};
    send_figure_U: {};

    // color and styles properties on shapes

    color_fill: string = 'rgba(200,100,50,0.7)'
    color_stroke: string = '#1f1'
    borders_size: number[] = [1, 2, 3, 4, 5, 6]
    border_size = 0


    station = 'AEDA'

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private dttServiceBinding: DataBindingService,
        private cpService: ColorPickerService

    ) {
        this.accepted = false;
        //dttServiceBinding.close()
        this.view = [this.width, this.height]
        this.gps_data = gps_list(this.amount_data)
    }


    ngOnInit() {
        var command = 'draw'

        this.realTimeData = true
        console.log(this.gps_data)

        console.log(this.data_serie)
        this.data_serie = this.gps2ds(this.gps_data)
        console.log(this.data_serie)

        //setInterval(this.updateData.bind(this), 1000)

        if (!this.fitContainer) {
            this.applyDimensions();
        }

        this.tohtml = JSON.stringify(this.data_serie)


        this.dttServiceBinding.stream_msg.
            subscribe(
            msg => {
                console.log(msg)
                if (msg.stream == 'station-data') {
                    console.log("Datashow")
                    if (msg.payload['data']['station_name'] == this.station) {
                        console.log("Sending new data...")
                        let new_gps = msg.payload["data"]
                        this.send_shape(new_gps)
                    }

                }
                else if (msg.payload['action'] == 'update') {
                    console.log("Nuevo dato")
                }

            })
    }

    gps2ds(gps) {
        var dataserie = {
            N: { name: gps[0].name, series: [] },
            E: { name: gps[0].name, series: [] },
            U: { name: gps[0].name, series: [] }
        }
        gps.forEach(d => {
            dataserie.N.series.push(d.N)
            dataserie.E.series.push(d.E)
            dataserie.U.series.push(d.U)

        })
        return dataserie

    }
    applyDimensions() {
        this.view = [this.width, this.height];
    }


    figure_base: boolean = true

    send_shape(new_gps) {
        console.log("Enviando figura")
        console.log(this.new_shape)
        this.selected_figure = 'circle'
        console.log(this.selected_figure)

        console.log(this.shape_list.indexOf(this.selected_figure))
        let figures: any[] = [];
        this.select_figures.forEach(d => { figures.push(d.value) })
        console.log(figures)
        //var new_gps = gps_list(1)[0]
        console.log(new_gps)
        var position_XY = {
            datetime: new_gps["timestamp"],
            value: new_gps["data"]["N"]['value'],
            min: new_gps["data"]["N"]["min"],
            max: new_gps["data"]["N"]["max"],
        }
        var figure = this.selected_figure;
        var attributes;
        var shape_styles = {
            fill: this.color_fill,
            stroke: this.color_stroke,
            stroke_width: this.border_size
        }

        if (this.selected_figure == 'circle') {
            attributes = {
                cx: 0,
                cy: 0,
                r: this.new_shape.radius
            }

        }

        else if (this.selected_figure == 'square') {
            attributes = {
                x1: 0,
                y1: 0,
                x2: this.new_shape.side_length,
                y2: this.new_shape.side_length,
            }
        }

        Object.assign(attributes, shape_styles)

        var shape_N = {
            figure_type: figure,
            properties: attributes,
            position_XY: position_XY
        }
        console.log("Colocando", shape_N)

        var position_XY = {
            datetime: new_gps["timestamp"],
            value: new_gps["data"]["E"]["value"],
            min: new_gps["data"]["E"]["min"],
            max: new_gps["data"]["E"]["max"],
        }
        var figure = this.selected_figure;
        var attributes;
        var shape_styles = {
            fill: this.color_fill,
            stroke: this.color_stroke,
            stroke_width: this.border_size
        }

        if (this.selected_figure == 'circle') {
            attributes = {
                cx: 0,
                cy: 0,
                r: this.new_shape.radius
            }

        }

        else if (this.selected_figure == 'square') {
            attributes = {
                x1: 0,
                y1: 0,
                x2: this.new_shape.side_length,
                y2: this.new_shape.side_length,
            }
        }

        Object.assign(attributes, shape_styles)


        var shape_E = {
            figure_type: figure,
            properties: attributes,
            position_XY: position_XY
        }
        console.log("Colocando", shape_E)

        var position_XY = {
            datetime: new_gps["timestamp"],
            value: new_gps["data"]["U"]["value"],
            min: new_gps["data"]["U"]["min"],
            max: new_gps["data"]["U"]["max"],
        }
        var figure = this.selected_figure;
        var attributes;
        var shape_styles = {
            fill: this.color_fill,
            stroke: this.color_stroke,
            stroke_width: this.border_size
        }

        if (this.selected_figure == 'circle') {
            attributes = {
                cx: 0,
                cy: 0,
                r: this.new_shape.radius
            }
        }

        else if (this.selected_figure == 'square') {
            attributes = {
                x1: 0,
                y1: 0,
                x2: this.new_shape.side_length,
                y2: this.new_shape.side_length,
            }
        }

        Object.assign(attributes, shape_styles)


        var shape_U = {
            figure_type: figure,
            properties: attributes,
            position_XY: position_XY
        }
        console.log("Colocando", shape_U)


        this.send_figure_N = shape_N;

        this.send_figure_E = shape_E;

        this.send_figure_U = shape_U;
    }

    showDiv(elem) {
        if (elem.value == 0) {
            document.getElementById('circle_form').style.display = "block";
        }
        else if (elem.value == 1) {
            document.getElementById('square_form').style.display = "block";
        }
    }

    selected_shape(shape) {
        console.log(this.selected_figure)
    }

    set_border_size() {
        console.log("Border size", this.border_size)
    }


    borderSelector: boolean = true

    showBorderSelector() {
        if (this.borderSelector) {
            this.borderSelector = false
            return this.borderSelector
        }
        else {
            this.borderSelector = true
            return this.borderSelector
        }
    }


}
