import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Import datat fro memory

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './mock-datatype';


// App components
import { SciInputComponent } from './sci_input.component';

// DataType class

import { DataType } from './components/datatype/elements/datatype'
// DataType Component
import { DataTypeComponent } from './components/datatype/main.component'
import { CreateCSKComponent } from './components/datatype/add.component';
import { ListDataTypesComponent } from './components/datatype/list.component';
import { DataTypeViewComponent } from './components/datatype/view.component';

// Other modules

import { SciInputRoutingModule } from './sci_input-routing.module'

//Services
//Reactive Forms

import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        SciInputComponent,
        CreateCSKComponent,
        ListDataTypesComponent,
        DataTypeViewComponent,
        DataTypeComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService),
        SciInputRoutingModule
    ],
    exports: [
        SciInputComponent,
        CreateCSKComponent,
        ListDataTypesComponent,
        DataTypeViewComponent,
        DataTypeComponent,
    ],
    providers: [],
})

export class SciInputModule { }
