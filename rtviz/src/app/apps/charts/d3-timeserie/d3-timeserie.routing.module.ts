import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// simple-timeserie components:
import { D3TimeSerieComponent } from './d3-timeserie.component'
// Axis components
// tick

import { XAxisTickComponent } from './ticks/x_tick.component'
import { TestXTickComponent } from './ticks/test-x-tick.component'
// ngx-charts
import { YAxisTickComponent } from './ticks/y_tick.component'
import { TestYTickComponent } from './ticks/test-y-tick.component'


const routes: Routes = [
    {
        path: 'test_x_tick',
        component: TestXTickComponent,
        children: [
        ]

    },
    {
        path: 'test_y_tick',
        component: TestYTickComponent,
        children: [
        ]

    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes, { enableTracing: true })],
    exports: [RouterModule]
})
export class D3TimeSerieRoutingModule { }

