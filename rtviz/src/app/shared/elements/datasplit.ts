export function dataNEU2splitNEU(new_data) {
    /*

      This function convert the structure of the data
      to ngx_charts data structure

      {name, series:{name, value, min, max}}
     */


    var station = new_data['station']
    var datetime = new_data['datetime']
    var variable = new_data['variable']


    var value = new_data['N']['value']
    var error = new_data['N']['error']

    var min_value = value - error
    var max_value = value + error

    var north = {
        name: station,
        serie: {
            name: datetime,
            value: value,
            min: min_value,
            max: max_value
        }
    }

    value = new_data['E']['value']
    error = new_data['E']['error']

    min_value = value - error
    max_value = value + error
    var east = {
        name: station,
        serie: {
            name: datetime,
            value: value,
            min: min_value,
            max: max_value
        }
    }

    value = new_data['U']['value']
    error = new_data['U']['error']

    min_value = value - error
    max_value = value + error

    var up = {
        name: station,
        serie: {
            name: datetime,
            value: value,
            min: min_value,
            max: max_value
        }
    }

    return [north, east, up]
}
