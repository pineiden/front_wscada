import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './templates/app.component.html',
    styleUrls: ['./static/app.component.css']
})
export class AppComponent {
    title = 'Real Time Visualization!';
}
