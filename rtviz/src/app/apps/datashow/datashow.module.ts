import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Import datat fro memory

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';


// App components
import { StationDataViewComponent } from './drawing_room.component';
import { ReactiveFormsModule } from '@angular/forms';

// d3 charts

import { D3_TimeserieChartModule } from '../charts/d3-timeserie/d3-timeserie.module'
import { ColorPickerModule } from 'angular4-color-picker';


//routing

import { DataShowRoutingModule } from './datashow-routing.module'

@NgModule({
    declarations: [
        StationDataViewComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        DataShowRoutingModule,
        D3_TimeserieChartModule,
        ColorPickerModule
    ],
    exports: [
        StationDataViewComponent
    ],
    providers: [],
})

export class DataShowModule { }
