import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';
import * as shape from 'd3-shape';
import * as d3 from 'd3';


// source of data
//import { data as countries } from 'emoji-flags';
//import { barChart, lineChartSeries } from './combo-chart-data';

@Component({
    selector: 'app-root',
    providers: [Location, { provide: LocationStrategy, useClass: HashLocationStrategy }],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'App';
    chart_name: string = 'time-serie';
    chart: any;
    chartType: string
    view: any[];
    width: number = 1200;
    height: number = 800;
    data: {};
    countries: any[];
    multi: any[];
    realTimeData: boolean = true;
    tooltipDisabled = true;

    gps_axis = 'N'

    linearScale: boolean = false;
    fitContainer: boolean = false;
    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    showXAxisLabel = true;
    xAxisLabel = 'TS';
    showYAxisLabel = true;
    yAxisLabel = 'N [m]';

    colorScheme = {
        domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };

    // line, area
    autoScale = true;

    chartGroups = [
        {
            name: 'Time Serie Chart',
            charts: [
                {
                    name: 'Time Serie Chart',
                    selector: 'time-serie',
                    options: [],
                    defaults: {}
                }
            ]
        },
    ]

    constructor(public location: Location) {
        this.view = [this.width, this.height]
        // load data to principal component
        console.log("Constructor")
        //Object.assign(this, { tree_example })
        //this.data = tree_example
        //Object.assign(this, { multi })
    }

    ngOnInit() {
        this.chartType = 'time-serie'
        this.realTimeData = true

        //setInterval(this.updateData.bind(this), 1000)

    }

    /*
      It's a function to test the way in that the GPS data is received

     */

    GPSGenerator() {
        var ts = new Date().getTime();
        var rand = Math.random();
        var Nvalue = rand * Math.sin(ts) * Math.exp(Math.cos(ts))
        var Evalue = rand * Math.cos(ts) * Math.exp(Math.sin(ts))
        var Uvalue = rand * Math.tan(ts) * Math.exp(Math.sin(Math.atanh(ts)))
        var error = rand * Math.random() * Math.random()
        var GPS = {
            TS: ts,
            N: { value: Nvalue, max_error: Nvalue + error, min_error: Nvalue - error },
            E: { value: Evalue, max_error: Evalue + error, min_error: Evalue - error },
            U: { value: Uvalue, max_error: Uvalue + error, min_error: Uvalue - error }
        }
        return GPS
    }

    updateData() {
        if (!this.realTimeData) {
            return;
        }
        const add = Math.random() < 0.7;
        const remove = Math.random() < 0.5;
        const gps = this.GPSGenerator();
        if (add) {
            // single
            const entry = gps.N
            this.multi = [...this.multi, entry];
        }

        console.log(this.multi)

    }

    applyDimensions() {
        this.view = [this.width, this.height];
    } x


    getFlag(country) {
        return this.countries.find(c => c.name === country).emoji;
    }

    selectChart(chartSelector) {
        this.chartType = chartSelector = chartSelector.replace('/', '');
        this.location.replaceState(this.chartType);

        for (const group of this.chartGroups) {
            this.chart = group.charts.find(x => x.selector === chartSelector);
            if (this.chart) break;
        }

        this.linearScale = false;
        this.yAxisLabel = 'N';
        this.xAxisLabel = 'Time';

        this.width = 700;
        this.height = 300;

        Object.assign(this, this.chart.defaults);

        if (!this.fitContainer) {
            this.applyDimensions();
        }
    }

    yLeftAxisScale(min, max) {
        return { min: `${min}`, max: `${max}` };
    }

    yRightAxisScale(min, max) {
        return { min: `${min}`, max: `${max}` };
    }

    yLeftTickFormat(data) {
        return `${data.toLocaleString()}`;
    }

    yRightTickFormat(data) {
        return `${data}%`;
    }

}
