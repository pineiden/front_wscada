import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Components

import { ChartsComponent } from './charts.component';

//Chart Components
import { AddChartComponent } from './components/chart/add.component';
import { ListChartsComponent } from './components/chart/list.component';
import { ViewChartComponent } from './components/chart/view.component';
//Chart's Creator Components
import { AddChartCreatorComponent } from './components/chart_creator/add.component';
import { ListChartsCreatorsComponent } from './components/chart_creator/list.component';
import { ViewChartCreatorComponent } from './components/chart_creator/view.component';

//Reactive Forms

import { ReactiveFormsModule } from '@angular/forms';

//Routing

import { ChartRoutingModule } from './charts-routing.module'

@NgModule({
    declarations: [
        ChartsComponent,
        AddChartComponent,
        ListChartsComponent,
        ViewChartComponent,
        AddChartCreatorComponent,
        ListChartsCreatorsComponent,
        ViewChartCreatorComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ChartRoutingModule

    ],
    exports: [
        ChartsComponent,
        AddChartComponent,
        ListChartsComponent,
        ViewChartComponent,
        AddChartCreatorComponent,
        ListChartsCreatorsComponent,
        ViewChartCreatorComponent
    ],
    providers: [],
})

export class ChartsModule { }
