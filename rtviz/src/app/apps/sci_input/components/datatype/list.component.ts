import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { DataType } from './elements/datatype';

import { WebSocketService } from '../../../../shared/services/websocket.service';
import { DataTypeBindingService } from './services/datatype_binding.service';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'list_datatypes',
    templateUrl: './templates/list.component.html',
    styleUrls: ['./static/css/list.component.css'],
    providers: [
        WebSocketService,
        DataTypeBindingService],

})

export class ListDataTypesComponent implements OnInit {
    title = 'Datatype\'s List';
    datatypes: DataType[] = [];
    //actualDTT: DataType;
    //selectedDataType: DataType;
    public idm: string;
    private accepted: boolean;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private dttServiceBinding: DataTypeBindingService
    ) {
        this.accepted = false;
        //dttServiceBinding.close()

    }

    ngOnInit(): void {
        console.log("Iniciando lista datatype")
        //this.dttService.connect()
        //this.getDataTypes()
        console.log("Datatypes from memory ok")
        //this.dttServiceBinding.connect()
        console.log("Websocket conectado")
        var command = 'list'
        this.dttServiceBinding.stream_msg.
            subscribe(
            msg => {
                if (msg.stream == 'datatype') {
                    if ('accept' in msg.payload) {
                        this.dttServiceBinding.acceptFlag(msg.payload['accept'])
                        this.idm = this.dttServiceBinding.getDataTypes()
                    }
                    var idm = this.idm
                    console.log("Mensaje input:")
                    console.log(msg)
                    var command_in = msg.payload["command"];
                    var idm_in = msg.payload['idm'];
                    //var dtt: DataType;
                    console.log("Esto ya llegó")
                    console.log(command_in)
                    console.log("IDM:")
                    console.log(idm)

                    if (command_in == command &&
                        this.dttServiceBinding.checkMSG(idm_in)) {
                        console.log("Cumple las condiciones")
                        var sdtt = msg.payload['message']['fields']
                        sdtt['id'] = msg.payload['message']['pk']
                        console.log("Elemento Datatype:")
                        //var new_dtt=sdtt['']
                        //this.actualDTT = new DataType(sdtt);
                        console.log("Print new datatype")
                        console.log(sdtt)
                        this.datatypes.push(sdtt)
                        //this.datatypes.push(sdtt)
                        /*

                        this.dtt['id'] = sdtt['pk']
                        this.dtt['name'] = sdtt['fields']['name']
                        this.dtt['csk'] = sdtt['fields']['csk']
                        this.dtt['description'] = sdtt['fields']['description']
                        this.dtt['type_di'] = sdtt['fields']['type_data_input']
                        this.dtt['graph'] = sdtt['graph']*/
                        //this.datatypes.push(sdtt)

                    }
                    //If UPDATE on PAYLOAD:
                    else if ('action' in msg.payload) {
                        if (msg.payload['action'] == 'update') {
                            //this.idm = this.dttServiceBinding.getDataType(rid)

                        }
                    }

                }
            });

        console.log("Reconectando y solicitando de nuevo list")
        //this.idm = this.dttServiceBinding.getDataTypes()

    }

    getDataTypes(): void {
        console.log("Extracting all datatypes")
        //this.dttService.getDataTypes().then(datatypes => this.datatypes = datatypes);
    }

    goBack(): void {
        this.location.back();
    }

}
