import { GPS } from "./gps"
import { GPSGenerator } from './gen_data'

export function gps_list(n: number): GPS[] {
    var gps_list: GPS[] = [];
    var list = Array.from(new Array(n), (x, i) => i)
    list.forEach(i => {
        var gps = GPSGenerator(i)
        console.log("New gps:", gps)
        gps_list.push(gps)
    })
    return gps_list
}
