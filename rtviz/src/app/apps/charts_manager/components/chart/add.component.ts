import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';


import 'rxjs/add/operator/switchMap';

// Forms
import {
    FormControl,
    FormGroup,
    Validator,
    Validators,
    ValidationErrors,
    CheckboxControlValueAccessor,
    CheckboxRequiredValidator
} from '@angular/forms';

@Component({
    selector: 'add_chart',
    templateUrl: './templates/add.component.html',
    styleUrls: ['./static/css/add.component.css'],
    providers: []
})

export class AddChartComponent implements OnInit {

    title = "Add Chart"

    ngOnInit(): void {
        console.log("Add Chart")
    }
}
