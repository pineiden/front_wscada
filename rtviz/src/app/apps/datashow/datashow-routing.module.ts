import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StationDataViewComponent } from './drawing_room.component';
//DataTypeComponent

const routes: Routes = [
    {
        path: 'datashow',
        component: StationDataViewComponent,
        children: [
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class DataShowRoutingModule { }

