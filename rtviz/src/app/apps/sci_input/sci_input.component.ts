import { Component } from '@angular/core';

@Component({
    selector: 'app-sci_input',
    templateUrl: './templates/sci_input.component.html',
    styleUrls: ['./static/css/sci_input.component.css']
})
export class SciInputComponent {
    title = 'Sci Input Datatype';
}
