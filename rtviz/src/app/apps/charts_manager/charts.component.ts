import { Component } from '@angular/core';

@Component({
    selector: 'app-charts',
    templateUrl: './templates/charts.component.html',
    styleUrls: ['./static/css/charts.component.css']
})
export class ChartsComponent {
    title = 'Charts Types';
}
