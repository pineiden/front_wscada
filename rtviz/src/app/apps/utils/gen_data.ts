import { GPS } from "./gps"


export function GPSGenerator(time_back: number): GPS {
    var station = "TESTGPS";
    var datetime = new Date();
    var dt = datetime.toISOString();
    var ts = datetime.getTime();
    var ts_gps = ts - time_back * 100e3;
    var rand = Math.random();
    var Nvalue = rand * Math.sin(ts) * Math.exp(Math.cos(ts))
    var Evalue = rand * Math.cos(ts) * Math.exp(Math.sin(ts))
    var Uvalue = rand * Math.tan(ts) * Math.exp(Math.sin(Math.tan(ts)))
    var error = rand * Math.random() * Math.random()
    var GPS_ = {
        name: station,
        TS: ts_gps,
        N: { datetime: dt, value: Nvalue, max: Nvalue + error, min: Nvalue - error, error: error },
        E: { datetime: dt, value: Evalue, max: Evalue + error, min: Evalue - error, error: error },
        U: { datetime: dt, value: Uvalue, max: Uvalue + error, min: Uvalue - error, error: error }
    }
    var gps = new GPS(GPS_);
    return gps
}
