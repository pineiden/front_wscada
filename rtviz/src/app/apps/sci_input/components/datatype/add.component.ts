import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { WebSocketService } from '../../../../shared/services/websocket.service';
import { DataTypeBindingService } from './services/datatype_binding.service';

import { Message } from '../../../../shared/elements/message'

import 'rxjs/add/operator/switchMap';

import { Viz } from 'd3plus-viz';

//
//import { requirejs } from 'requirejs';



const Nidm = 5;


import {
    FormControl,
    FormGroup,
    Validator,
    Validators,
    ValidationErrors,
    CheckboxControlValueAccessor,
    CheckboxRequiredValidator
} from '@angular/forms';


@Component({
    selector: 'add_datatype',
    templateUrl: './templates/add.component.html',
    styleUrls: ['./static/css/add.component.css'],
    providers: [
        WebSocketService,
        DataTypeBindingService]
})

export class CreateCSKComponent implements OnInit {

    title: string;
    command: string;
    up_or_save: string;
    dtt_form: FormGroup;
    refresh: string;
    //public graph_viz = new Viz();
    //requirejs = requirejs;
    public rid: string;
    public dtt = {};
    public msg_list = new Map();
    public msg_req = new Map();
    private msg = {
        idm: '',
        command: '',
        checked: false,
        message: '',
        data: Object,
        model: '',
        action: '',
        pk: '',
    };
    private idm: string;
    private accepted: boolean;
    public result: string;

    messages: Message[] = [];

    public form_names = {
        'name': "Name of Datatype",
        'csk': "Compact String Keys",
        'description': "Describe data input",
    }

    constructor(
        //public datatype: DataType,
        //public graph: string,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private dttService: DataTypeBindingService,
    ) {

        //dttService.close()
        this.accepted = false;
        this.title = "Datatype edition"
        this.route.params.subscribe(params => {
            console.log("Parametros:")
            console.log(params)
            if ('id' in params) {
                this.title = "Update Datatype id:" + params['id']
                this.rid = params['id']
                this.command = 'update'
            }
            else {
                this.title = "Create a new Datatype"
                this.command = 'save'
            }
        })

        if (this.command == 'update') {
            this.refresh = 'Refresh'
            this.up_or_save = 'Update'

        }
        else {
            this.refresh = 'Clean'
            this.up_or_save = 'Save'
        }

    }

    ngOnInit(): void {
        console.log("Loading form to create or update datatype")
        //Define REACTIVE FORM
        this.dtt_form = new FormGroup({
            name: new FormControl(this.form_names['name'],
                [Validators.required,
                Validators.minLength(5),
                Validators.maxLength(100)]),
            csk: new FormControl(this.form_names['csk'],
                [Validators.required,
                Validators.minLength(10),
                Validators.maxLength(300)]),
            description: new FormControl(this.form_names['description'],
                [Validators.required,
                Validators.minLength(10),
                Validators.maxLength(500)]),
            check: new FormControl(false, ),
        });


        //Define WS connection to receive data:
        var command = 'get_id'
        this.dttService.stream_msg.subscribe(msg => {
            //do action
            console.log("Mensaje RAW")
            console.log(msg)

            var idm = this.idm
            var accepted = msg.payload['accept']
            this.accepted = accepted
            var command_in = msg.payload['command']
            var idm_in = msg.payload['idm']

            //Show result msg about some action
            if ('message' in msg.payload) {
                if ('result' in msg.payload['message']) {
                    console.log(msg.payload['message']['result'])
                    this.result = msg.payload['message']['result']
                }
            }
            //Start sending msg
            if ('accept' in msg.payload) {
                this.dttService.acceptFlag(msg.payload['accept'])
                this.getDTT()
            }
            //Get data from json
            else if (command_in == command && idm_in == idm) {
                console.log("Cumple las condiciones")
                var sdtt = msg.payload['message']

                this.dtt['id'] = sdtt['pk']

                if (this.dtt['id'] == this.rid) {
                    this.dtt['name'] = sdtt['fields']['name']
                    this.dtt['csk'] = sdtt['fields']['csk']
                    this.dtt['description'] = sdtt['fields']['description']
                    this.dtt['type_di'] = sdtt['fields']['type_data_input']
                    this.dtt['graph'] = sdtt['graph']
                    this.updateForm()
                    console.log("FORM FIELDS")
                    console.log(this.form_names)
                }
                else {
                    this.result = "No existe este Datatype"
                }

            }


            //If UPDATE on PAYLOAD:
            if ('action' in msg.payload) {
                if (msg.payload['action'] == 'update') {
                    this.idm = this.dttService.getDataType(this.rid)

                }
                else if (msg.payload['action'] == 'delete') {
                    console.log(msg.payload)
                    this.cleanDTT()
                }
            }
            //
            //this.messages.push(msg)
            /*
            var command = msg.command
            var idm = msg.idm
            if (command == 'check') {
                console.log("Es una respuesta a chequeo")
                var graph_viz = new Viz();
                if (this.msg_list.has(idm)) {
                    var gm = msg.message
                    var graph = gm['graph']
                    console.log('El graph:')
                    console.log(graph)
                    console.log(typeof (graph_viz))
                    var connections = [
                        { "source": "alpha", "target": "beta", "strength": 1.25 },
                        { "source": "alpha", "target": "gamma", "strength": 2.463 },
                        { "source": "beta", "target": "delta", "strength": 0.823 },
                        { "source": "beta", "target": "epsilon", "strength": 1.563 },
                        { "source": "zeta", "target": "gamma", "strength": 3.125 },
                        { "source": "theta", "target": "gamma", "strength": 0.732 },
                        { "source": "eta", "target": "gamma", "strength": 2.063 }
                    ]

                    graph_viz
                        .data(connections)
                        //.color('green')
                        //.select('#csk_graph')
                        //.label('CSK Graph')
                        .render()
                    //.downloadButton()

                    //graph_viz.render()

                }               
        }
        */

            console.log(msg)
        }
        )

        console.log("Loading requirejs")
        console.log(Viz)
        //console.log(graph_viz)
        /**
        requirejs.config({
            baseUrl: "static/js",
            paths: {
                d3: bower_path.concat("d3/d3"),
                "dot-checker": bower_path.concat('graphviz-d3-renderer/dist/dot-checker'),
                "layout-worker": bower_path.concat('graphviz-d3-renderer/dist/layout-worker'),
                worker: bower_path.concat('requirejs-web-workers/src/worker'),
                renderer: bower_path.concat('graphviz-d3-renderer/dist/renderer')
            }
        });
        **/
        console.log("Require values:")

    }


    send(command: string, message) {
        var key = this.msg_send_idm(Nidm)
        this.msg.idm = key
        this.msg.command = command
        this.msg.message = message
        this.msg.checked = this.dtt_form.get('check').value
        this.msg_list.set(key, this.msg)
        console.log("Enviando a server")
        console.log(this.msg)
        //this.dttService.stream_msg.next(this.msg)
    }

    updateForm() {
        this.form_names['name'] = this.dtt['name']
        this.form_names['csk'] = this.dtt['csk']
        this.form_names['description'] = this.dtt['description']
        console.log("Updating values on form")
        this.dtt_form.patchValue(this.form_names)
        //this.dtt_form.controls['name'].updateValueAndValidity(this.form_names['name'])
        //this.dtt_form.controls['csk'].updateValueAndValidity(this.form_names['csk'])
        //this.dtt_form.controls['description'].updateValueAndValidity(this.form_names['description'])

    }

    clean() {
        console.log("Cleaning...")
        if (this.command == 'update') {
            this.dtt_form.patchValue(this.form_names)
        }
        else {
            var form_names = {
                'name': '',
                'csk': '',
                'description': ''
            }
            this.dtt_form.patchValue(form_names)
        }

    }

    makeid(n: number) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < n; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    msg_send_idm(n: number) {
        var idm = this.makeid(n)
        while (this.msg_list.has(idm)) {
            idm = this.makeid(n)
        }
        return idm
    }


    goBack(): void {
        this.location.back();
    }

    save() {
        var datatype = {}
        datatype['id'] = this.rid
        datatype['name'] = this.dtt_form.get('name').value
        datatype['csk'] = this.dtt_form.get('csk').value
        datatype['description'] = this.dtt_form.get('description').value


        console.log("Enviando datos a " + this.command)
        var message = {}

        message['datatype'] = datatype
        message['checked'] = this.dtt_form.get('check').value

        if (this.command == 'update') {
            console.log("Updating on server")
            this.dttService.send(this.command, 'datatype', message)

        }
        else {
            console.log("Saving on server")
            this.dttService.send(this.command, 'datatype', message)

        }

    }

    checkCSK(): void {
        //this.dttService.check(this.datatype.csk).then(graph => this.graph = graph);
        console.log(this.dtt_form.get('csk').value)
        var message = {};
        message['csk'] = this.dtt_form.get('csk').value
        console.log("Checking value")
        console.log(message)
        this.dttService.send('check', 'datatype', message)
    }

    isUnChecked(): boolean { return false; }

    delete(): void {
    }

    /*    create_graph(graph) {
            require['renderer'], function(renderer) {
                var dotSource = graph;
                renderer.init({ element: "#csk_graph", extend: [0.1, 10] });
                console.log("Rendering graph")
                renderer.render(dotSource)
            }
        }
    */
    getDTT() {
        console.log("Se acepto connexion:")
        console.log(this.accepted)
        this.idm = this.dttService.getDataType(this.rid)
    }

    deleteDTT() {
        console.log("Delete ", this.rid)
        this.dttService.deleteDataType(this.rid)
    }

    cleanDTT() {
        // for all properties
        for (const prop of Object.getOwnPropertyNames(this.dtt)) {
            delete this.dtt[prop];
        }
    }

}
