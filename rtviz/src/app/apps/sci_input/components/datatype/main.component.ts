import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';


import { DataTypeBindingService } from './services/datatype_binding.service';

@Component({
    selector: 'view_datatype',
    templateUrl: './templates/main.component.html',
    styleUrls: ['./static/css/main.component.css']
})

export class DataTypeComponent {
    title = 'Datatype';

    constructor(
        private dttService: DataTypeBindingService,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit(): void {
    }

    goBack(): void {
    }

}
