import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { DataType } from './elements/datatype';

import { WebSocketService } from '../../../../shared/services/websocket.service';
import { DataTypeBindingService } from './services/datatype_binding.service';

import { Message } from '../../../../shared/elements/message'

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/first';

import { Subject } from 'rxjs/Subject';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

import { Viz } from 'd3plus-viz';

const Nidm = 5;

@Component({
    selector: 'add_datatype',
    templateUrl: './templates/view.component.html',
    styleUrls: ['./static/css/view.component.css'],
    providers: [
        WebSocketService,
        DataTypeBindingService]
})


export class DataTypeViewComponent implements OnInit {

    title = "View Datatype "
    public msg_list = new Map();
    public idm: string;
    private idd: number;
    public rid: string;
    public subject: Subject<MessageEvent>;
    public result: string;

    private msg = {
        idm: '',
        command: '',
        checked: false,
        message: '',
        data: Object,
        model: '',
        action: '',
        pk: '',
    };
    //messages: Message[] = [];
    //msg_req: Message[] = [];

    // handle datatype input from route url. view/:id
    dtt_json: {};

    public dtt = {};
    private accepted: boolean;
    //private datatype: DataType;

    private value: Object;
    private anyErrors: boolean;
    private finished: boolean;
    private connection: Object;


    constructor(
        //private firstWSS: DataTypeBindingService,
        private dttServiceBinding: DataTypeBindingService,
        private route: ActivatedRoute,
        private location: Location) {

        this.result = ''

        this.accepted = false;

        //dttServiceBinding.close()


        console.log("Stream service:")
        dttServiceBinding.stream()

        var rid = this.route.snapshot.paramMap.get('id')

        var command = 'get_id';

        this.dttServiceBinding.stream_msg.
            subscribe(
            msg => {
                if (msg.stream == 'datatype') {
                    var idm = this.idm
                    console.log("Mensaje input:")
                    console.log(msg)
                    var accepted = msg.payload['accept']
                    this.accepted = accepted
                    var command_in = msg.payload["command"];
                    var idm_in = msg.payload['idm'];
                    //var dtt: DataType;
                    console.log("Esto ya llegó")
                    console.log(command_in)
                    console.log("IDM:")
                    console.log(idm)
                    if ('message' in msg.payload) {
                        if ('result' in msg.payload['message']) {
                            console.log(msg.payload['message']['result'])
                            this.result = msg.payload['message']['result']
                        }
                    }
                    if ('accept' in msg.payload) {
                        this.dttServiceBinding.acceptFlag(msg.payload['accept'])
                        this.getDTT()
                    }

                    else if (command_in == command && idm_in == idm) {
                        console.log("Cumple las condiciones")
                        var sdtt = msg.payload['message']

                        this.dtt['id'] = sdtt['pk']

                        if (this.dtt['id'] == this.rid) {
                            this.dtt['name'] = sdtt['fields']['name']
                            this.dtt['csk'] = sdtt['fields']['csk']
                            this.dtt['description'] = sdtt['fields']['description']
                            this.dtt['type_di'] = sdtt['fields']['type_data_input']
                            this.dtt['graph'] = sdtt['graph']
                        }
                        else {
                            this.result = "No existe este Datatype"
                        }

                    }

                    //If UPDATE on PAYLOAD:
                    if ('action' in msg.payload) {
                        if (msg.payload['action'] == 'update') {
                            this.idm = this.dttServiceBinding.getDataType(rid)

                        }
                        else if (msg.payload['action'] == 'delete') {
                            console.log(msg.payload)
                            this.cleanDTT()
                        }
                    }


                }
            });

    }

    ngOnInit(): void {
        // get id from route and load to datatpye
        console.log("Extrayendo ID de ruta")



        var rid = this.route.snapshot.paramMap.get('id')
        this.rid = rid
        console.log("ID Datatype:")
        console.log(rid)
        //var idm = this.dttService.getDataType(rid)
        var command = 'get_id';
        var sdtt: string;
        console.log("Iniciando vista de elemento")

        this.getDTT()

        console.log("Primera solicitud envianda")

    }

    goBack(): void {
        this.location.back();
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getDTT() {
        console.log("Se acepto connexion:")
        console.log(this.accepted)
        this.idm = this.dttServiceBinding.getDataType(this.rid)
    }

    deleteDTT() {
        console.log("Delete ", this.rid)
        this.dttServiceBinding.deleteDataType(this.rid)
    }

    cleanDTT() {
        // for all properties
        for (const prop of Object.getOwnPropertyNames(this.dtt)) {
            delete this.dtt[prop];
        }
    }

}
