export interface Message {
    stream: string,
    payload: Object
}
