import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

// Forms
import {
    FormControl,
    FormGroup,
    Validator,
    Validators,
    ValidationErrors,
    CheckboxControlValueAccessor,
    CheckboxRequiredValidator
} from '@angular/forms';

@Component({
    selector: 'add_chart_creator',
    templateUrl: './templates/add.component.html',
    styleUrls: ['./static/css/add.component.css'],
    providers: []
})

export class AddChartCreatorComponent implements OnInit {

    title = "Add Chart's Creator"

    ngOnInit(): void {
        console.log("Add Chart's Creator")
    }
}
