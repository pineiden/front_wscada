export class DataType {
    id = 0;
    name = '';
    csk = '';
    description = '';
    type_di = '';
    graph?= Object;

    constructor(dtt) {
        console.log("Creating new datatype")
        this.id = dtt['id']
        this.name = dtt['name']
        this.description = dtt['description']
        this.type_di = dtt['type_di']
        if ('graph' in dtt) {
            this.graph = dtt['graph']
        }
    }

    public dtt_name() {
        return this.name
    }

    public tree() {
        return this.graph
    }
}
