import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Components
import { AppComponent } from './app.component';

import { PageNotFoundComponent } from './not-found.component';

// Apps modules
import { SciInputModule } from './apps/sci_input/sci_input.module';
import { ChartsModule } from './apps/charts_manager/charts.module';
import { DataShowModule } from './apps/datashow/datashow.module'
import { AppRoutingModule } from './app-routing.module'

//Services
import { WebSocketService } from './shared/services/websocket.service';

//Animations
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent
    ],
    imports: [
        //BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        SciInputModule,
        ChartsModule,
        DataShowModule,
        AppRoutingModule
    ],
    exports: [],
    providers: [WebSocketService],
    bootstrap: [AppComponent]
})
export class AppModule { }
