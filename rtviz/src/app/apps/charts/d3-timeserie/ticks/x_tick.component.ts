import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    OnInit,
    ElementRef,
    ViewChild,
    SimpleChanges,
    AfterViewInit,
    ChangeDetectionStrategy
} from '@angular/core';

import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';


import { timeFormat } from 'd3-time-format';


@Component({
    selector: 'g[tick-axis-x]',
    templateUrl: './x_tick.component.html',
    styleUrls: ['./x_tick.styles.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('animationState', [
            transition(':leave', [
                style({
                    opacity: 1,
                }),
                animate(500, style({
                    opacity: 0
                }))
            ])
        ])
    ]
})
export class XAxisTickComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() label;
    @Input() position;
    @Input() y1;
    @Input() y2;
    @Input() axis_y;
    @Input() axis_x;
    @Input() tick_styles;
    // input to enable or disable grid or tick group
    @Input() on_grid: boolean = true;
    @Input() on_tick: boolean = true;
    @Input() dash_style: any = {
        dash: "2,3,2",
        width: 1,
        stroke: "gray"
    }

    @Input() line_style: any = {
        width: 2,
        stroke: "black"
    }

    @Input() tick_length: number = 30;

    // text position
    pos_text: {
        x: number,
        y: number,
    };

    label_style: string;


    // tick positions

    tick_grid = {}
    tick_line = {}

    @Input() font_size_label = 15
    @Input() text_anchor = "middle"//{middle, start, end}

    // date format

    tick_label: string = "";
    @Input() dateFormat: string = "%H:%M:%S"

    tick_timeFormat: any;

    constructor() { }

    ngOnInit() {
        this.tick_timeFormat = timeFormat(this.dateFormat)
        this.tick_label = this.tick_timeFormat(this.label)
        this.set_y_position()
        console.log("Axis x", this.axis_y)
        console.log("Axis y", this.axis_x)
        this.getTickSize()
        console.log("tick grid", this.tick_grid)
        console.log("tick line", this.tick_line)
        console.log("pos_text", this.pos_text)
        console.log("Dash grid", this.dash_style)
        console.log("Line style", this.dash_style)

    }

    // after first
    ngAfterViewInit() {

    }

    // when changes
    ngOnChanges() {
        console.log("Change")
        this.tick_timeFormat = timeFormat(this.dateFormat)
        this.tick_label = this.tick_timeFormat(this.label)
        this.set_y_position()
        this.getTickSize()

    }

    tickTransform() {
        if (this.position) {
            return "translate(" + this.position + ",0)"
        }
    }

    setDashStyle(name, value) {
        this.dash_style = value
    }

    setLabelTransform(value) {
        this.label_style = value
    }


    set_y_position() {
        this.y1 = this.axis_y["y1"]
        this.y2 = this.axis_y["y2"]
    }

    getTickSize() {
        // tick grid
        this.tick_grid = {
            x1: 0,
            y1: this.y1,
            x2: 0,
            y2: this.y2
        }
        // tick line
        this.tick_line = {
            x1: 0,//this.position,
            y1: this.y2,
            x2: 0,//this.position,
            y2: this.y2 + this.tick_length
        }
        this.pos_text = {
            x: 0,//this.position,
            y: this.y2 + this.tick_length + this.font_size() + 2,
        }

    }

    gridEnabled() {
        return true
    }

    tickEnabled() {
        return true
    }

    font_size() {
        return this.font_size_label
    }

    get_text_anchor() {
        return this.text_anchor
    }

}
