import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { QueueingSubject } from 'queueing-subject'

// Reference; https://channels.readthedocs.io/en/stable/javascript.html
// CSKConsumer: check, create, update, delete
// route: sci_input/csk/
import 'rxjs/add/operator/toPromise';

import { WebSocketService } from '../../../shared/services/websocket.service'
import { Message } from '../../../shared/elements/message';

import { Subject } from 'rxjs/Subject';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';



const URL_STREAM = 'ws://localhost:8000/station/data-stream/'
const Nidm = 5;

@Injectable()
export class DataBindingService {
    public msg_list = new Map();
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private datatypesUrl = 'api/datatypes';  // URL to web api
    //private wsc = new WebSocketClient(this.url)
    //Subject https://github.com/Reactive-Extensions/RxJS/blob/master/doc/gettingstarted/subjects.md
    public stream_msg: Subject<Message> = new Subject<Message>();

    private accepted: boolean;

    private msg = {
        stream: '',
        payload: {
            idm: '',
            command: '',
            checked: false,
            message: ''
        },
    };

    private payload = {
        idm: '',
        command: '',
        checked: false,
        message: ''
    }

    constructor(
        private http: Http,
        private wssServiceStream: WebSocketService,
        //private wsb: WebSocketBridge,
    ) {
        this.stream()
        //this.connect()
        // manage messages from stream
        this.stream_msg = <Subject<Message>>wssServiceStream
            .connect(URL_STREAM)
            .map((response: MessageEvent): Message => {
                console.log("MSG Stream")
                console.log(response.data)
                let data = JSON.parse(response.data)
                console.log(data)
                console.log(typeof (data))
                console.log("Commando a enviar")
                console.log(data.payload['command'])
                return {
                    stream: data.stream,
                    payload: data.payload
                }
            });
    };

    acceptFlag(flag) {
        this.accepted = flag
    }

    stream() {
        console.log(this.wssServiceStream)
    }

    connect() {
        console.log("Connecting to websocket Stream Binding")
        return this.wssServiceStream.connect(URL_STREAM)
    }

    makeid(n: number) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < n; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    msg_send_idm(n: number) {
        var idm = this.makeid(n)
        while (this.msg_list.has(idm)) {
            idm = this.makeid(n)
        }
        return idm
    }

    send(command: string, stream: string, message) {
        this.msg.stream = stream
        var key = this.msg_send_idm(Nidm)
        this.payload.idm = key
        this.payload.command = command
        this.payload.message = message
        if ('checked' in message) {
            this.payload.checked = message['checked']
        }
        this.msg.payload = this.payload
        this.msg_list.set(key, this.msg)
        console.log("Enviando a server")
        console.log(this.msg)
        if (this.accepted) {
            this.stream_msg.next(this.msg)
        }
        else {
            console.log("No hay conexión aceptada")
        }
        console.log("ENVIO OK")
        console.log(key)
        return key
    }


    getDataType(id: string) {
        var sdtt: string;
        var command = 'get_id';
        var dtt_id = +id;
        var message = {}
        message['id'] = dtt_id
        console.log("Obteniendo requerimiento de id " + dtt_id)
        var idm = this.send(command, 'datatype', message)
        //dtt to datatype
        return idm
    }

    getDataTypes() {
        var command = 'list'
        var message = {}
        console.log("Solicitando lista completa de datatype")
        var idm = this.send(command, 'datatype', message)
        return idm
    }

    checkMSG(idm): boolean {
        if (this.msg_list.has(idm)) {
            return true
        }
        else {
            return false
        }
    }

    public close() {
        this.stream_msg.unsubscribe()
        console.log("Socket closed")
    }

    deleteDataType(id: string) {
        var sdtt: string;
        var command = 'delete';
        var dtt_id = +id;
        var message = {}
        message['id'] = dtt_id
        var idm = this.send(command, 'datatype', message)
        //dtt to datatype
        return idm
    }


}
