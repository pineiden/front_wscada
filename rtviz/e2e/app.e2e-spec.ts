import { RtvizPage } from './app.po';

describe('rtviz App', () => {
  let page: RtvizPage;

  beforeEach(() => {
    page = new RtvizPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
